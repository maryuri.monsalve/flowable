package com.pragma.vacaciones.servicio.solicitud;

import com.pragma.vacaciones.manejador.comando.CrearProcesoComando;
import com.pragma.vacaciones.manejador.respuesta.RespuestaBase;
import com.pragma.vacaciones.manejador.respuesta.RespuestaEstadoSolicitud;

import java.util.List;

public interface SolicitudServicio {
    public RespuestaBase solicitarVacaciones(CrearProcesoComando crearProcesoComando);
    public List<RespuestaEstadoSolicitud> consultarSolicitudes(String idAprovador);
}
