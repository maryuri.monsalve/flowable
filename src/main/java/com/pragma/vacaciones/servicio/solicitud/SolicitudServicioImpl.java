package com.pragma.vacaciones.servicio.solicitud;

import com.pragma.vacaciones.manejador.comando.CrearProcesoComando;
import com.pragma.vacaciones.manejador.respuesta.RespuestaBase;
import com.pragma.vacaciones.manejador.respuesta.RespuestaEstadoSolicitud;
import lombok.RequiredArgsConstructor;
import org.flowable.engine.RuntimeService;
import org.flowable.engine.TaskService;
import org.flowable.engine.runtime.ProcessInstance;
import org.flowable.task.api.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@RequiredArgsConstructor
public class SolicitudServicioImpl implements SolicitudServicio {

    @Autowired
    private RuntimeService runtimeService;

    @Autowired
    private TaskService taskService;

    @Override
    public RespuestaBase solicitarVacaciones(CrearProcesoComando crearProcesoComando) {
        Map<String, Object> variables = crearVariables(crearProcesoComando);
        ProcessInstance processInstance = runtimeService.startProcessInstanceByKey("vacacionesProceso", variables);
        return RespuestaBase.ok(processInstance.getId());
    }

    @Override
    public List<RespuestaEstadoSolicitud> consultarSolicitudes(String idAprovador) {
        List<Task> tasks =
                taskService.createTaskQuery().taskAssignee(idAprovador).list();
        List<RespuestaEstadoSolicitud> respuestaEstadoSolicitudes = getTaskDetails(tasks);
        return respuestaEstadoSolicitudes;
    }

    public Map<String,Object> crearVariables(CrearProcesoComando crearProcesoComando){
        Map<String, Object> variables = new HashMap<String, Object>();
        variables.put("usuario", crearProcesoComando.getUsuario());
        variables.put("aprobador", crearProcesoComando.getAprobador());
        variables.put("vicepresidencia", crearProcesoComando.getVicepresidencia());
        variables.put("pais", crearProcesoComando.getPais());
        variables.put("fechaInicio", crearProcesoComando.getFechaInicio());
        variables.put("fechaFin", crearProcesoComando.getFechaFin());
        variables.put("cantidadDias", crearProcesoComando.getCantidadDias());
        variables.put("observaciones", crearProcesoComando.getObservaciones());
        return variables;
    }

    private List<RespuestaEstadoSolicitud> getTaskDetails(List<Task> tasks) {
        List<RespuestaEstadoSolicitud> respuestaEstadoSolicitudes = new ArrayList<>();
        for (Task task : tasks) {
            Map<String, Object> processVariables = taskService.getVariables(task.getId());
            respuestaEstadoSolicitudes.add(new RespuestaEstadoSolicitud(task.getId(), task.getName(), processVariables));
        }
        return respuestaEstadoSolicitudes;
    }
}
