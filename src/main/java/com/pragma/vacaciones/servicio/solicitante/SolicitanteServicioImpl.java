package com.pragma.vacaciones.servicio.solicitante;

import com.pragma.vacaciones.manejador.dto.SolicitanteDto;
import com.pragma.vacaciones.manejador.mapper.SolicitanteMapper;
import com.pragma.vacaciones.persistencia.entidad.SolicitanteEntidad;
import com.pragma.vacaciones.persistencia.repositorio.SolicitanteRepositorio;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class SolicitanteServicioImpl implements SolicitanteServicio {

    @Autowired
    private final SolicitanteRepositorio solicitanteRepositorio;


    @Override
    public List<SolicitanteDto> ListarSolicitantes() {
        List<SolicitanteEntidad> solicitantes = solicitanteRepositorio.findAll();
        List<SolicitanteDto> solicitantesDto=SolicitanteMapper.MAPPER.haciaDtos(solicitantes);
        return solicitantesDto;
    }

    @Override
    public SolicitanteDto obtenerSolicitante(Integer id) {
        SolicitanteEntidad solicitanteEntidad = solicitanteRepositorio.findById(id).orElse(new SolicitanteEntidad());
        SolicitanteDto solicitanteDto = SolicitanteMapper.MAPPER.entidadHaciaDto(solicitanteEntidad);
        return solicitanteDto;
    }

    @Override
    public SolicitanteDto crearSolicitante(SolicitanteDto solicitanteDto) {
        SolicitanteEntidad solicitanteEntidad = SolicitanteMapper.MAPPER.dtoHaciaEntidad(solicitanteDto);
        solicitanteEntidad.setEstado("Creado");
        SolicitanteEntidad solicitanteEntidad1 = solicitanteRepositorio.save(solicitanteEntidad);
        return SolicitanteMapper.MAPPER.entidadHaciaDto(solicitanteEntidad1);
    }

    @Override
    public SolicitanteDto actualizarSolicitante(SolicitanteDto solicitanteDto) {
        SolicitanteEntidad solicitanteEntidad = SolicitanteMapper.MAPPER.dtoHaciaEntidad(solicitanteDto);
        solicitanteRepositorio.save(solicitanteEntidad);
        return solicitanteDto;
    }

    @Override
    public SolicitanteDto borrarSolicitante(Integer id) {
        SolicitanteDto solicitanteDto = obtenerSolicitante(id);
        SolicitanteEntidad solicitanteEntidad = SolicitanteMapper.MAPPER.dtoHaciaEntidad(solicitanteDto);
        solicitanteEntidad.setEstado("Eliminado");
        solicitanteRepositorio.save(solicitanteEntidad);
        return solicitanteDto;
    }
}
