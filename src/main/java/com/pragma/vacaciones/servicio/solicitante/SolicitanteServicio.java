package com.pragma.vacaciones.servicio.solicitante;

import com.pragma.vacaciones.manejador.dto.SolicitanteDto;
import com.pragma.vacaciones.persistencia.entidad.SolicitanteEntidad;

import java.util.List;
import java.util.Optional;

public interface SolicitanteServicio {
    public List<SolicitanteDto> ListarSolicitantes();
    public SolicitanteDto obtenerSolicitante(Integer id);
    public SolicitanteDto crearSolicitante(SolicitanteDto solicitanteDto);
    public SolicitanteDto actualizarSolicitante(SolicitanteDto solicitanteDto);
    public  SolicitanteDto borrarSolicitante(Integer id);
}
