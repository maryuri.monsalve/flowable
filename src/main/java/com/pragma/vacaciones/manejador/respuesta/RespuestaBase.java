package com.pragma.vacaciones.manejador.respuesta;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class RespuestaBase implements Serializable {

    private String mensaje;

    public static RespuestaBase ok(String mensaje) {
        return new RespuestaBase(mensaje);
    }
}
