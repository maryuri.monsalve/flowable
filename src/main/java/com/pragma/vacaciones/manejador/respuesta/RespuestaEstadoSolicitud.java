package com.pragma.vacaciones.manejador.respuesta;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class RespuestaEstadoSolicitud {

    private String taskId;
    private String taskName;
    private Map<String, Object> taskData;
}
