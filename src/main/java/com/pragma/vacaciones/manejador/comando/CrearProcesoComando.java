package com.pragma.vacaciones.manejador.comando;

import lombok.Getter;

import java.time.LocalDate;

@Getter
public class CrearProcesoComando {
    private String usuario;
    private String aprobador;
    private String vicepresidencia;
    private String pais;
    private LocalDate fechaInicio;
    private LocalDate fechaFin;
    private int cantidadDias;
    private String observaciones;
    private LocalDate fechaCreacion;

}
