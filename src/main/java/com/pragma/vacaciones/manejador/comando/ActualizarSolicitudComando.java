package com.pragma.vacaciones.manejador.comando;

import lombok.Data;

import java.time.LocalDate;

@Data
public class ActualizarSolicitudComando {

    private String id;
    private String idUsuario;
    private String idAprobador;
    private String idVicepresidencia;
    private String idPais;
    private LocalDate fechaInicio;
    private LocalDate fechaFin;
    private int cantidadDias;
    private String observaciones;
    private LocalDate fechaCreacion;
}
