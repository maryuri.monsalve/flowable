package com.pragma.vacaciones.manejador.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SolicitanteDto {
    private String id;
    private String idVicepresidencia;
    private String idPais;
    private String nombre;
    private String apellido;
    private int cantidadDias;
    private String cedula;
    private String correo;
    private String rol;
}
