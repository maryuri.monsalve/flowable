package com.pragma.vacaciones.manejador.mapper;

import com.pragma.vacaciones.manejador.dto.SolicitanteDto;
import com.pragma.vacaciones.persistencia.entidad.SolicitanteEntidad;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;
import java.util.Optional;

@Mapper
public interface SolicitanteMapper {

    SolicitanteMapper MAPPER = Mappers.getMapper(SolicitanteMapper.class);

    SolicitanteEntidad dtoHaciaEntidad (SolicitanteDto solicitanteDto);

    SolicitanteDto entidadHaciaDto (SolicitanteEntidad solicitanteEntidad);

    List<SolicitanteEntidad> haciaEntidades(List<SolicitanteDto> solicitanteDtoList);

    List<SolicitanteDto> haciaDtos(List<SolicitanteEntidad> solicitanteEntidadList);

}
