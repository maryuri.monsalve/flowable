package com.pragma.vacaciones.persistencia.repositorio;

import com.pragma.vacaciones.persistencia.entidad.SolicitanteEntidad;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SolicitanteRepositorio extends JpaRepository<SolicitanteEntidad,Integer> {


}
