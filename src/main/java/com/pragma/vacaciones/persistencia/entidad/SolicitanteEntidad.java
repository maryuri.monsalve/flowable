package com.pragma.vacaciones.persistencia.entidad;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class SolicitanteEntidad {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private int id;
    private String idVicepresidencia;
    private String idPais;
    private String nombre;
    private String apellido;
    private int cantidadDias;
    private String cedula;
    private String correo;
    private String rol;
    private String estado;

}
