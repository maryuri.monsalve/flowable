package com.pragma.vacaciones.controlador;

import com.pragma.vacaciones.manejador.comando.CrearProcesoComando;
import com.pragma.vacaciones.manejador.dto.SolicitanteDto;
import com.pragma.vacaciones.manejador.respuesta.RespuestaBase;
import com.pragma.vacaciones.manejador.respuesta.RespuestaEstadoSolicitud;
import com.pragma.vacaciones.servicio.solicitud.SolicitudServicio;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequiredArgsConstructor
@RestController
@RequestMapping(value="/solicitudes")
public class controladorSolicitudes {

    @Autowired
    private final SolicitudServicio solicitudServicio;

    @PostMapping
    public ResponseEntity<RespuestaBase> solicitarVacaciones(@RequestBody CrearProcesoComando crearProcesoComando){
        RespuestaBase respuestaBase = solicitudServicio.solicitarVacaciones(crearProcesoComando);
        return ResponseEntity.status(HttpStatus.CREATED).body(respuestaBase);
    }

    @GetMapping(value="/{idAprobador}")
    public ResponseEntity<List<RespuestaEstadoSolicitud>> consultarSolicitudes(@PathVariable("idAprobador") String idAprobador){
        List<RespuestaEstadoSolicitud> respuestaEstadoSolicitudes = solicitudServicio.consultarSolicitudes(idAprobador);
        if(respuestaEstadoSolicitudes.equals(null)) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(respuestaEstadoSolicitudes);
    }
}
