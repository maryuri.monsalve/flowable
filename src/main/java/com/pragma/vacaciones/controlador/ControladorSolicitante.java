package com.pragma.vacaciones.controlador;

import com.pragma.vacaciones.manejador.dto.SolicitanteDto;
import com.pragma.vacaciones.servicio.solicitante.SolicitanteServicio;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequiredArgsConstructor
@RestController
@RequestMapping(value="/solicitantes")
public class ControladorSolicitante {

    @Autowired
    private final SolicitanteServicio solicitanteServicio;

    @GetMapping
    public ResponseEntity<List<SolicitanteDto>> listarSolicitantes(){
        List<SolicitanteDto> solicitantes=solicitanteServicio.ListarSolicitantes();
        if(solicitantes.isEmpty()) {
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.ok(solicitantes);
    }

    @GetMapping(value="/{id}")
    public ResponseEntity<SolicitanteDto> obtenerSolicitante(@PathVariable("id") Integer id){
        SolicitanteDto solicitanteDto = solicitanteServicio.obtenerSolicitante(id);
        if(solicitanteDto.equals(null)) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(solicitanteDto);
    }

    @PostMapping
    public ResponseEntity<SolicitanteDto> crearSolicitante(@RequestBody SolicitanteDto solicitanteDto){
        SolicitanteDto solicitanteDto1=solicitanteServicio.crearSolicitante(solicitanteDto);
        return ResponseEntity.status(HttpStatus.CREATED).body(solicitanteDto1);
    }

    @PutMapping
    public ResponseEntity<SolicitanteDto> actualizarSolicitante(@RequestBody SolicitanteDto solicitanteDto){
        SolicitanteDto solicitanteDto1=solicitanteServicio.actualizarSolicitante(solicitanteDto);
        if(solicitanteDto.equals(null)) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(solicitanteDto1);
    }

    @DeleteMapping(value="/{id}")
    public ResponseEntity<SolicitanteDto> borrarSolicitante(@PathVariable("id") Integer id){
        SolicitanteDto solicitanteDto = solicitanteServicio.borrarSolicitante(id);
        if(solicitanteDto.equals(null)) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(solicitanteDto);
    }
}
