package com.pragma.vacaciones.ejecucion;

import org.flowable.engine.delegate.DelegateExecution;
import org.flowable.engine.delegate.JavaDelegate;

public class SolicitudRechazada implements JavaDelegate {

    @Override
    public void execute(DelegateExecution execution) {

        System.out.println("Rejected, sending an email");
    }
}
